import java.util.Scanner;
public class LuckyCardGameApp {
	public static void main(String[] args) {
		Deck deckCards = new Deck();
		deckCards.shuffle();
		Scanner sc = new Scanner(System.in);
		System.out.println("Welcome User! How many cards would you like to draw? ");
		int cardsToDraw = sc.nextInt();
		while (cardsToDraw < 0 || cardsToDraw > 52) {
			System.out.println("Please enter a valid amount: ");
			cardsToDraw = sc.nextInt();
		}
		System.out.println(deckCards.length());
		deckCards.drawTopCard(cardsToDraw);
		System.out.println(deckCards.length());
	}
}