import java.util.Random;
public class Deck {
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	public Deck() {
		this.rng = new Random();
		this.numberOfCards = 52;
		this.cards = new Card[this.numberOfCards];
		String[] suits = {"Spades", "Heart", "Clubs", "Diamond"};
		String[] values = {"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};
		int index = 0;
		for (int i = 0; i < values.length; i++) {
            for (int x = 0; x < suits.length; x++) {
				cards[index] = new Card(values[i], suits[x]);
				index++;
			}
		}
	}
	public int length() {
		return this.numberOfCards;
	}
	public Card drawTopCard(int cardsToDraw) {
		this.numberOfCards = numberOfCards-cardsToDraw;
		return cards[this.numberOfCards];
	}
	public String toString() {
		String builder = "";
		for (Card card : this.cards) {
			builder = builder + card + "\n"; 
		}
		return builder;
	}
	public void shuffle() {
		for (int i = 0; i < this.numberOfCards; i++) {
			int random = rng.nextInt(numberOfCards);
			Card swap = cards[i];
			cards[i] = cards[random];
			cards[random] = swap;
		}
	}
}